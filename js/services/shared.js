'use strict';
var app = angular.module('SmartCity');

app.factory("SharedVars", function(Map) {

	return {
		data_graph : function(e){
			if (e==undefined){
				return {
					consumo_electrico: [0],
					nivel_luz: [0],
					temperatura: [0],
					casa_uuid: null
				}
			}
			Map.getSpark(e)
			.success(function(data){
				//console.log(data.data[0].consumo_electrico);
			    return {
					consumo_electrico: [
								data.data[0].consumo_electrico,
								data.data[1].consumo_electrico,
								data.data[2].consumo_electrico,
								data.data[3].consumo_electrico,
								data.data[4].consumo_electrico,
								data.data[5].consumo_electrico,
								data.data[7].consumo_electrico,
								data.data[8].consumo_electrico,
								data.data[9].consumo_electrico
							],
			        nivel_luz: [
								data.data[0].nivel_luz,
								data.data[1].nivel_luz,
								data.data[2].nivel_luz,
								data.data[3].nivel_luz,
								data.data[4].nivel_luz,
								data.data[5].nivel_luz,
								data.data[7].nivel_luz,
								data.data[8].nivel_luz,
								data.data[9].nivel_luz
							],
			        temperatura: [
								data.data[0].temperatura,
								data.data[1].temperatura,
								data.data[2].temperatura,
								data.data[3].temperatura,
								data.data[4].temperatura,
								data.data[5].temperatura,
								data.data[7].temperatura,
								data.data[8].temperatura,
								data.data[9].temperatura
							],
			        casa_uuid: e
			    }
			});
		}
	}


});
