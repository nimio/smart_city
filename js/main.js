$("#left-tab").click(function() {
    $("i", this).toggleClass("rotated");
    $("#left-sidebar").toggleClass("open");
    if ($("#left-sidebar").hasClass("open"))
        $(this).animate({
            "padding-left": "2px"
        }, 1000);
    else
        $(this).animate({
            "padding-left": "6px"
        }, 1000, function() {
            $(this).css("padding-left", "");
        });
});
