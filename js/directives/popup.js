'use strict';
var app = angular.module('SmartCity');

app.directive('popup', function() {
    return {
        restrict: 'A',
        templateUrl: 'popup.html',
        scope: {},
        controller: function($scope, $timeout, SharedVars, Map) {

            $scope.getContent = function(uuid) {
                $scope.uuid = uuid;
                // Simulate a call to the server to get data
                $timeout(function() {
                    Map.get(uuid)
                        .success(function(data) {
                            $scope.content = data.data[0];
                        });
                    // $scope.content = GetContentService.getContent;
                    // $scope.data_graph = SharedVars.data_graph(uuid);
                    // console.log($scope.data_graph);
                    // $scope.data_graph.consumo_electrico = [1,0,3,-3,5]
                    Map.getSpark(uuid)
                        .success(function(data) {
                            var datos = data.data.reverse()
                            //console.log(data.data[0].consumo_electrico);
                            $scope.data_graph = {
                                consumo_electrico: [],
                                nivel_luz: [],
                                temperatura: [],
                                casa_uuid: uuid
                            }

                            for (var i = 0; i < datos.length; i++) {
                                $scope.data_graph.consumo_electrico.push(datos[i].consumo_electrico);
                                $scope.data_graph.temperatura.push(datos[i].temperatura);
                                $scope.data_graph.nivel_luz.push(datos[i].nivel_luz);
                            };
                        });
                }, 200)
            }
        }
    }
});
