'use strict';

var app = angular.module('SmartCity', [
        'mapMod',
        'casasMod',
        'ngAnimate',
        'ngRoute'
    ])
    .constant('DBUrl', 'http://test.nim.io/hm_smartcity')
    .config(function($routeProvider) {
        $routeProvider
            .when('/index', {
                templateUrl: '../index.html',
                controller: 'mapControl'
            })
            .otherwise({
                redirectTo: '/'
            });
    });
