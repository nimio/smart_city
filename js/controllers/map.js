'use strict';
 var app = angular.module('SmartCity');

app.controller("mapControl",function($scope,Map,Casas,$compile,$interval) {
    // Datos devueltos por web2py
    // @param float nivel_luz
    // @param float temperatura
    // @param float consumo_electrico
    // @param init casa_uuid

    $scope.mediciones = [];
    $scope.casas = [];
    $scope.casasSeleccionadas = {
        casas_uuid: [],
        casas_leaflet_id: []
    };

    $scope.dynamicsparkline = [19,23,14,23];

    Map.get()
    .success(function(data){
        // $scope.mediciones = data.data[0];
    	$scope.mediciones = data;
    });

    /***  little hack starts here ***/
    L.Map = L.Map.extend({
        openPopup: function(popup) {
            //        this.closePopup();  // just comment this
            this._popup = popup;

            return this.addLayer(popup).fire('popupopen', {
                popup: this._popup
            });
        }
    }); /***  end of hack ***/

    var CasasLayer = new L.LayerGroup();



 	var icons = L.divIcon({
        className: 'glyphicon glyphicon-home parpadeo',
        iconSize:[30,30]
        });

    // create a map in the "map" div, set the view to a given place and zoom
    var map = L.map("map",{closePopupOnClick:false}).setView(
                        [-36.818938, -73.050319],
                         11);

    map.addLayer(CasasLayer);

    // add an OpenStreetMap tile layer
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

    Casas.get()
    .success(function(data){
        $scope.casas = data;
    });

    $scope.BuscaLeafletId = function(leaflet_id){
        return $scope.casasSeleccionadas.casas_leaflet_id.indexOf(leaflet_id);

    }

    $scope.BuscaUUID = function(casa_uuid){
        return $scope.casasSeleccionadas.casas_uuid.indexOf(casa_uuid);
    };

    $scope.ToggleCasas = function(casa_uuid, latitud, longitud){

        var indice = $scope.BuscaUUID(casa_uuid);

        if(indice>-1){
            var leaflet_id = $scope.casasSeleccionadas.casas_leaflet_id[indice];
            $scope.casasSeleccionadas.casas_uuid.splice(indice,1);
            $scope.casasSeleccionadas.casas_leaflet_id.splice(indice,1);
            //map.getLayer(leaflet_id).closePopup();
            CasasLayer.removeLayer(leaflet_id);
        } else{
            var newScope = $scope.$new();
            var e = $compile('<div popup></div>')(newScope);
            var casa = L.marker(
                [latitud, longitud],
                {icon: icons,
                 uuid: casa_uuid
                })
                .addTo(CasasLayer);
            casa.bindPopup(e[0]);
            casa.on('click',function(){
                angular.element(e).scope().$$childHead.getContent(casa_uuid);
                $interval(function(){
                    angular.element(e).scope().$$childHead.getContent(casa_uuid);
                },3000)
            });
            $scope.casasSeleccionadas.casas_uuid.push(casa_uuid);
            $scope.casasSeleccionadas.casas_leaflet_id.push(casa._leaflet_id);

        }

        console.info($scope.casasSeleccionadas);
    };


});
